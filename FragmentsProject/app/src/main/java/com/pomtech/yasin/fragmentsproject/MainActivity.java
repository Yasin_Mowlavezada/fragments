package com.pomtech.yasin.fragmentsproject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.graphics.drawable.RippleDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    static int counter = 1;
    Button back , next;
    Fragment fragment;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        next = (Button) findViewById(R.id.next);

        back = (Button) findViewById(R.id.back);


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragment = fragmentCreator(counter);

                fragmentTransaction.replace(R.id.picture_layout, fragment);

//                back.setText(counter + "");

                if (counter != 10) {
                    ++counter;
                } else {
                    counter = 1;
                }
                fragmentTransaction.commit();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                if (counter != 1) {
                    --counter;
                } else {
                    counter = 10;
                }

                fragment = fragmentCreator(counter);

                fragmentTransaction.replace(R.id.picture_layout, fragment);

//                back.setText(counter + "");

                fragmentTransaction.commit();
            }
        });
    }

    public Fragment fragmentCreator(int counter) {
//        back.setText(counter + "");
        Fragment fragment = null;

        if (counter == 1) {
            fragment = new FirstFragment();
        } else if (counter == 2) {
            fragment = new SecondFragment();
        } else if (counter == 3) {
            fragment = new ThirdFragment();
        } else if (counter == 4) {
            fragment = new FourthFragment();
        } else if (counter == 5) {
            fragment = new FifthFragment();
        } else if (counter == 6) {
            fragment = new SixthFragment();
        } else if (counter == 7) {
            fragment = new SeventhFragment();
        } else if (counter == 8) {
            fragment = new EighthFragment();
        } else if (counter == 9) {
            fragment = new NinthFragment();
        } else if (counter == 10) {
            fragment = new TenthFragment();
        }

        return fragment;
    }
}
