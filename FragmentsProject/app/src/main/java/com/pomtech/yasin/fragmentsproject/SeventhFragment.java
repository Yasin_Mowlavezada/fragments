package com.pomtech.yasin.fragmentsproject;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by M.Yasin on 4/10/2017.
 */

public class SeventhFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.seventh , container ,false);
        if (v != null)
            return v;

        return container;
    }
}
